import 'package:flutter/material.dart';

import 'my_image.dart';

class ButtonManager extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ButtonManagerState();
  }
}

class _ButtonManagerState extends State<ButtonManager> {
  int counter=0;
  final List<Alignment> alingments = [
    new Alignment(-1.0, -1.0),
    new Alignment(0.0, -1.0),
    new Alignment(1.0, -1.0),
    new Alignment(1.0, -0.5),
    new Alignment(1.0, 0.0),
    new Alignment(1.0, 0.5),
    new Alignment(1.0, 1.0),
    new Alignment(0.0, 1.0),
    new Alignment(-1.0, 1.0),
    new Alignment(-1.0, 0.5),
    new Alignment(-1.0, 0.0),
    new Alignment(-1.0, -0.5),
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[MyImage(alingments[counter]),
        Container(
          margin: EdgeInsets.all(10.0),
          child: RaisedButton(
            color: Theme.of(context).accentColor,
            child: Text("Pozisyon Değiştir"),
            onPressed: () {
              setState(() {
                counter++;
                counter=counter%12;
              });
            },
          ),
        ),
      ],
    );
  }

}