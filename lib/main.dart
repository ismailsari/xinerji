import 'package:flutter/material.dart';

import './button_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Demo';


    return MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.amber
      ),
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body:ButtonManager()
      ),
    );
  }
}
