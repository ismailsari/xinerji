import 'package:flutter/material.dart';

class MyImage extends StatelessWidget {
  final Alignment alignment;
  MyImage(this.alignment);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: Image.asset(
          "assets/Face.svg",
          height: 100.0,
        ),
        alignment:alignment,
      ),
    );
  }


}